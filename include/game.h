#ifndef _GAME_H_
#define _GAME_H_

enum ChessManType {
    PAWN,
    BISHOP,
    KNIGHT,
    ROOK,
    ROOK_CASTLEABLE,
    QUEEN,
    KING,
    KING_CASTLEABLE
};

enum Player {
    BLACK,
    WHITE,
    NONE
};

struct ChessMan {
    Player player;
    ChessManType type;
};

class Game {
public:
    Game();
    ~Game();

    /*
     * Moves the piece at [currCol, currRow] to [newCol, newRow].
     * Returns 0 on success, -1 on failure.
     */
    int movePiece(int currCol, int currRow, int newCol, int newRow);
    bool isValidMove(int currCol, int currRow, int newCol, int newRow);
    bool hasWon(Player player);
    void printBoard();

private:
    bool isValidMovePawn(int currCol, int currRow, int newCol, int newRow, Player player);
    bool isValidMoveBishop(int currCol, int currRow, int newCol, int newRow);
    bool isValidMoveKnight(int currCol, int currRow, int newCol, int newRow);
    bool isValidMoveRook(int currCol, int currRow, int newCol, int newRow);
    bool isValidMoveQueen(int currCol, int currRow, int newCol, int newRow);
    bool isValidMoveKing(int currCol, int currRow, int newCol, int newRow);
    bool kingIsInCheck(int kingCol, int kingRow);

    struct ChessMan board[8][8];
    int whiteKing[2];
    int blackKing[2];
    Player currPlayer;
};

#endif /* _GAME_H_ */