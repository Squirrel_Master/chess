#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>

void printChessMan(const ChessMan *chessMan);

Game::Game() : currPlayer(WHITE),
               whiteKing{3, 0},
               blackKing{3, 7} {

    for (int row = 2; row < 6; row++) {
        for (int col = 0; col < 8; col++) {
            board[row][col].player = NONE;
        }
    }

    /* Place pawns. */
    for (int col = 0; col < 8; col++) {
        board[1][col].type = PAWN;
        board[1][col].player = WHITE;
        board[6][col].type = PAWN;
        board[6][col].player = BLACK;
    }

    /* Place remaining white pieces. */
    board[0][0].type = ROOK_CASTLEABLE;
    board[0][0].player = WHITE;
    board[0][1].type = KNIGHT;
    board[0][1].player = WHITE;
    board[0][2].type = BISHOP;
    board[0][2].player = WHITE;
    board[0][3].type = KING_CASTLEABLE;
    board[0][3].player = WHITE;
    board[0][4].type = QUEEN;
    board[0][4].player = WHITE;
    board[0][5].type = BISHOP;
    board[0][5].player = WHITE;
    board[0][6].type = KNIGHT;
    board[0][6].player = WHITE;
    board[0][7].type = ROOK_CASTLEABLE;
    board[0][7].player = WHITE;

    /* Place remaining black pieces. */
    board[7][0].type = ROOK_CASTLEABLE;
    board[7][0].player = BLACK;
    board[7][1].type = KNIGHT;
    board[7][1].player = BLACK;
    board[7][2].type = BISHOP;
    board[7][2].player = BLACK;
    board[7][3].type = KING_CASTLEABLE;
    board[7][3].player = BLACK;
    board[7][4].type = QUEEN;
    board[7][4].player = BLACK;
    board[7][5].type = BISHOP;
    board[7][5].player = BLACK;
    board[7][6].type = KNIGHT;
    board[7][6].player = BLACK;
    board[7][7].type = ROOK_CASTLEABLE;
    board[7][7].player = BLACK;
}

Game::~Game() {
}

bool Game::isValidMove(int currCol, int currRow, int newCol, int newRow) {

    /* Ensure the coordinates are new and within the board. */
    if (currCol < 0 || currCol > 7 ||
        currRow < 0 || currRow > 7 ||
        newCol < 0 || newCol > 7 ||
        newRow < 0 || newRow > 7 ||
        (currCol == newCol && currRow == newRow)) {
        return false;
    }

    /* Ensure the moving player is WHITE or BLACK. */
    struct ChessMan *chessMan = &(board[currRow][currCol]);
    if (chessMan->player == NONE) {
        return false;
    }

    /* Ensure the player cannot attack itself. */
    if (board[newRow][newCol].player == chessMan->player) {
        return false;
    }

    switch (chessMan->type) {
    case PAWN:
        return isValidMovePawn(currCol, currRow, newCol, newRow, chessMan->player);
    case BISHOP:
        return isValidMoveBishop(currCol, currRow, newCol, newRow);
    case KNIGHT:
        return isValidMoveKnight(currCol, currRow, newCol, newRow);
    case ROOK:
    case ROOK_CASTLEABLE:
        return isValidMoveRook(currCol, currRow, newCol, newRow);
    case QUEEN:
        return isValidMoveQueen(currCol, currRow, newCol, newRow);
    case KING:
    case KING_CASTLEABLE:
        return isValidMoveKing(currCol, currRow, newCol, newRow);
    }

    return false;
}

int Game::movePiece(int currCol, int currRow, int newCol, int newRow) {

    if (!isValidMove(currCol, currRow, newCol, newRow)) {
        return -1;
    }

    struct ChessMan *newChessMan = &(board[newRow][newCol]);
    struct ChessMan *oldChessMan = &(board[currRow][currCol]);
    struct ChessMan origNewChessMan = *newChessMan;
    newChessMan->player = oldChessMan->player;
    newChessMan->type = oldChessMan->type;
    oldChessMan->player = NONE;

    /* Rollback move if the current player is in check. */
    if (currPlayer == WHITE ? kingIsInCheck(whiteKing[0], whiteKing[1]) : kingIsInCheck(blackKing[0], blackKing[1])) {
        *oldChessMan = *newChessMan;
        *newChessMan = origNewChessMan;
        return -1;
    }

    if (newChessMan->type == KING_CASTLEABLE) {
        newChessMan->type = KING;
    } else if (newChessMan->type == ROOK_CASTLEABLE) {
        newChessMan->type = ROOK;
    }

    currPlayer = currPlayer == WHITE ? BLACK : WHITE;
    return 0;
}

bool Game::hasWon(Player player) {
    return false;
}

void Game::printBoard() {
    printf(" ");
    for (int i = 0; i < 8; i++) {
        printf(" %c", i + 'a');
    }
    printf("\n");
    for (int row = 0; row < 8; row++) {
        printf("%d", row + 1);
        for (int col = 0; col < 8; col++) {
            printf("|");
            ChessMan *chessMan = &(board[row][col]);
            if (chessMan->player == NONE) {
                printf(" ");
            } else {
                printChessMan(chessMan);
            }

        }
        printf("\n");
    }
}

void printChessMan(const ChessMan *chessMan) {
#ifdef __unix__
    /* Set unicode to the unicode value for a white/black king in chess. */
    int unicode = chessMan->player == WHITE ? 0x265A : 0x2654;
    switch (chessMan->type) {
    case PAWN:
        unicode += 5;
        break;
    case BISHOP:
        unicode += 3;
        break;
    case KNIGHT:
        unicode += 4;
        break;
    case ROOK:
    case ROOK_CASTLEABLE:
        unicode += 2;
        break;
    case QUEEN:
        unicode += 1;
        break;
    case KING:
    case KING_CASTLEABLE:
    default:
        break;
    }

    setlocale(LC_CTYPE, "");
    printf("%lc", unicode);
#else
    const char *token = chessMan->player == WHITE ? "pbnrqk" : "PBNRQK";
    switch (chessMan->type) {

    /* White: 'p' Black: 'P' */
    case PAWN:
        printf("%c", token[0]);
        break;

    /* White: 'b' Black: 'B' */
    case BISHOP:
        printf("%c", token[1]);
        break;

    /* White: 'n' Black: 'N' */
    case KNIGHT:
        printf("%c", token[2]);
        break;

    /* White: 'r' Black: 'R' */
    case ROOK:
    case ROOK_CASTLEABLE:
        printf("%c", token[3]);
        break;

    /* White: 'q' Black: 'Q' */
    case QUEEN:
        printf("%c", token[4]);
        break;

    /* White: 'k' Black: 'K' */
    case KING:
    case KING_CASTLEABLE:
        printf("%c", token[5]);
        break;
    }
#endif
}

bool Game::isValidMovePawn(int currCol, int currRow, int newCol, int newRow, Player player) {
    int dir;
    int startRow;
    if (player == WHITE) {
        dir = 1;
        startRow = 1;
    } else {
        dir = -1;
        startRow = 6;
    }

    /* Can move 1 or 2 tiles on the first move only, otherwise only 1 tile. */
    if (currCol == newCol) {

        /* Ensure only diagonal attacks. */
        if (board[newRow][newCol].player != NONE) {
            return false;
        }

        /* Do not jump other chessmen on initial 2-tile move. */
        if ((currRow == startRow) && (newRow == startRow + (2 * dir))) {
            return board[startRow + dir][currCol].player == NONE;
        }

    /* Diagonal attack. */
    } else if ((newCol != currCol + 1 && newCol != currCol - 1) || board[newRow][newCol].player == NONE) {
        return false;
    }

    /* Only move 1 row. */
    return newRow == currRow + dir;
}

bool Game::isValidMoveBishop(int currCol, int currRow, int newCol, int newRow) {

   /* Ensure the move is diagonal. */
    if (abs(currCol - newCol) != abs(currRow - newRow)) {
        return false;
    }

    /* Ensure no chessmen are hopped. */
    if (newRow < currRow) {
        newRow ^= currRow;
        currRow ^= newRow;
        newRow ^= currRow;
    }
    if (newCol < currCol) {
        newCol ^= currCol;
        currCol ^= newCol;
        newCol ^= currCol;
    }
    for (int i = currRow + 1; i < newRow; i++) {
        for (int j = currCol + 1; j < newCol; j++) {
            if (board[i][j].player != NONE) {
                return false;
            }
        }
    }

    return true;
}

bool Game::isValidMoveKnight(int currCol, int currRow, int newCol, int newRow) {
    int colDiff = abs(currCol - newCol);
    int rowDiff = abs(currRow - newRow);
    return (colDiff == 1 && rowDiff == 2) || (colDiff == 2 && rowDiff == 1);
}

bool Game::isValidMoveRook(int currCol, int currRow, int newCol, int newRow) {

    /* Vertical move. */
    if (currCol == newCol) {
        if (newRow < currRow) {
            newRow ^= currRow;
            currRow ^= newRow;
            newRow ^= currRow;
        }

        /* Ensure no chessmen are hopped. */
        while (++currRow < newRow) {
            if (board[currRow][currCol].player != NONE) {
                return false;
            }
        }

    /* Horizontal move. */
    } else if (currRow == newRow) {
        if (newCol < currCol) {
            newCol ^= currCol;
            currCol ^= newCol;
            newCol ^= currCol;
        }

        /* Ensure no chessmen are hopped. */
        while (++currCol < newCol) {
            if (board[currRow][currCol].player != NONE) {
                return false;
            }
        }

    /* Invalid diagonal movement detected. */
    } else {
        return false;
    }

    return true;
}

bool Game::isValidMoveQueen(int currCol, int currRow, int newCol, int newRow) {
    return isValidMoveRook(currCol, currRow, newCol, newRow) || isValidMoveBishop(currCol, currRow, newCol, newRow);
}

bool Game::isValidMoveKing(int currCol, int currRow, int newCol, int newRow) {
    int rowDiff = abs(currRow - newRow);
    int colDiff = abs(currCol - newCol);
    if (rowDiff <= 1 && colDiff <= 1) {
        return true;
    }

    /*
     * Check for castling:
     * 1. must be first move for king and the chosen rook
     * 2. cannot castle out of check
     * 3. cannot castle through check (is there a space between king and rook that is exposed to enemy?)
     * 4. No pieces can be between king and the rook
     * 5. king can move either 2 tiles left or right. Rook goes on opposite side of king
     */
    struct ChessMan *king = &(board[currRow][currCol]);
    if (king->type != KING_CASTLEABLE || colDiff != 2) {
        return false;
    }

    /* Ensure the rook is eligible for castling. */
    struct ChessMan *rook;
    if (newCol < currCol) {
        rook = &(board[currRow][0]);
        newCol ^= currCol;
        currCol ^= newCol;
        newCol ^= currCol;
    } else {
        rook = &(board[currRow][7]);
    }

    if (rook->type != ROOK_CASTLEABLE) {
        return false;
    }
    while (currCol <= newCol) {
        if (kingIsInCheck(currCol++, currRow)) {
            return false;
        }
    }

    return true;
}

bool Game::kingIsInCheck(int kingCol, int kingRow) {
    // iterate through all pieces, see if they have a valid move to king
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            if (isValidMove(col, row, kingCol, kingRow)) {
                return true;
            }
        }
    }
    return false;
}