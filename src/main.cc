#include <stdio.h>
#include "game.h"

int main(int argc, char **argv) {
    Game game;

    for (;;) {
        game.printBoard();
        int currCol;
        int currRow;
        int newCol;
        int newRow;
        while (true) {
            printf("Enter move: ");
            char currPos[3];
            char newPos[3];
            scanf("%2s %2s", currPos, newPos);
            printf("\nYou entered %s->%s\n", currPos, newPos);

            currCol = currPos[0];
            currRow = currPos[1];
            newCol = newPos[0];
            newRow = newPos[1];

            if (currCol < currRow) {
                currCol ^= currRow;
                currRow ^= currCol;
                currCol ^= currRow;
            }

            if (newCol < newRow) {
                newCol ^= newRow;
                newRow ^= newCol;
                newCol ^= newRow;
            }

            currRow -= '1';
            newRow -= '1';

            if (currRow < 0 || currRow > 7 || newRow < 0 || newRow > 7) {
                printf("Row must be 1-8\n");
                continue;
            }

            currCol = currCol >= 'a' ? currCol - 'a' : currCol - 'A';
            newCol = newCol >= 'a' ? newCol - 'a' : newCol - 'A';
            if (currCol < 0 || currCol > 7 || newCol < 0 || newCol > 7) {
                printf("Column must be a-h\n");
                continue;
            }

            break;
        }
        printf("coords: [%d,%d]->[%d,%d]\n", currCol, currRow, newCol, newRow);
        if (game.movePiece(currCol, currRow, newCol, newRow) < 0) {
            printf("Unable to move piece.\n");
        }
    }
    return 0;
}