# CHESS #

### Required Software ###
* cmake

### Build and Run ###
1. Create new build directory within the root directory.
2. Go to the build directory.
3. Create makefile with CMake:
> cmake ..
4. Compile:
> make
5. Run:
> ./chess